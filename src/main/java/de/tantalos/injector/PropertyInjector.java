package de.tantalos.injector;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.google.common.base.Preconditions;

public class PropertyInjector {


    /**
     * Injecting fields by final annotation defined configuration
     *
     * @param object
     * @throws UnsatisfiedPropertyInjection
     * @throws
     * @throws IllegalConfigurationException
     */
    public void inject(Object object) throws UnsatisfiedPropertyInjection {
        try {
            final File configFile = getConfigurationByAnnotation(object);
            FileConfiguration yml = YamlConfiguration.loadConfiguration(configFile);
            if (yml == null) {

            }
            inject0(object, yml);
        } catch (IOException | URISyntaxException e) {
            throw new UnsatisfiedPropertyInjection("Error reading configruation. Injection of class " + object.getClass()
                    + "failed");
        }
    }

    /**
     * Injecting fields with dynamic configuration
     *
     * @param object
     * @param properties
     * @throws IllegalConfigurationException
     */
    public void inject(Object object, FileConfiguration configuration) throws UnsatisfiedPropertyInjection {
        Preconditions.checkNotNull(configuration);
        Preconditions.checkNotNull(object);
        try {
            inject0(object, configuration);
        } catch (IOException | URISyntaxException e) {
            throw new UnsatisfiedPropertyInjection("Error reading configruation. Injection of class "
                    + object.getClass() + "failed");
        }
    }

    private File getConfigurationByAnnotation(Object object) throws URISyntaxException, UnsatisfiedPropertyInjection {
        final Class<?> objectClass = object.getClass();
        final PropertyFile propertyFile = objectClass.getAnnotation(PropertyFile.class);

        String fileName = propertyFile.name();
        if(fileName.charAt(0) != '/') {
            fileName = "/" + fileName;
        }

        URL url = object.getClass().getResource(fileName);
        if(url == null) {
            throw new UnsatisfiedPropertyInjection(
                    "Can not find configuration defined in " + propertyFile + " annotation");
        }
        return new File(url.toURI());
    }

    private void inject0(Object object, FileConfiguration configuration) throws IOException, URISyntaxException,
            UnsatisfiedPropertyInjection {
        final Class<?> objectClass = object.getClass();

        for (final Field field : objectClass.getDeclaredFields()) {
            final Property property = field.getAnnotation(Property.class);

            if (property == null) {
                continue;
            }
            final String propertyName = property.name();
            setValue(object, field, configuration, propertyName);
        }
    }

    private void setValue(Object instance, Field field, FileConfiguration configuration, String propertyName)
            throws UnsatisfiedPropertyInjection {
        final Class<?> type = field.getType();
        field.setAccessible(true);

        if (!configuration.isSet(propertyName)) {
            throw new UnsatisfiedPropertyInjection("Unsatisfied class " + instance.getClass()
                    + ". No value is matching the key " + propertyName + " of field " + field.getName()
                    + ". Please check your config");
        }

        final Object value = configuration.get(propertyName);

        try {
            if (type.isAssignableFrom(value.getClass())) {
                field.set(instance, type.cast(value));
            } else if (type.isPrimitive()) {
                final String valueAsString = value.toString();
                if (type.getTypeName().equals("int")) {
                    final int intValue = Integer.valueOf(valueAsString);
                    field.set(instance, intValue);
                } else if (type.getTypeName().equals("long")) {
                    final long intValue = Long.valueOf(valueAsString);
                    field.set(instance, intValue);
                } else if (type.getTypeName().equals("byte")) {
                    final byte intValue = Byte.valueOf(valueAsString);
                    field.set(instance, intValue);
                } else if (type.getTypeName().equals("short")) {
                    final short intValue = Short.valueOf(valueAsString);
                    field.set(instance, intValue);
                } else if (type.getTypeName().equals("double")) {
                    final double intValue = Double.valueOf(valueAsString);
                    field.set(instance, intValue);
                } else if (type.getTypeName().equals("float")) {
                    final float intValue = Float.valueOf(valueAsString);
                    field.set(instance, intValue);
                } else if (type.getTypeName().equals("char")) {
                    final char intValue = valueAsString.charAt(0);
                    field.set(instance, intValue);
                } else if (type.getTypeName().equals("boolean")) {
                    final boolean intValue = Boolean.valueOf(valueAsString);
                    field.set(instance, intValue);
                } else {
                    throw new UnsatisfiedPropertyInjection("Unsatisfied " + instance.getClass()
                            + ". Cannot apply property " + propertyName + "=(" + value.getClass() + ") " + value
                            + " to the field (" + type + ") " + field.getName());
                }
            } else {
                throw new IllegalArgumentException("Type of value" + value.getClass()
                        + " in configuration is not responsive to filed type " + type);
            }
        } catch (ClassCastException | IllegalArgumentException e) {
            throw new UnsatisfiedPropertyInjection("Unsatisfied " + instance.getClass() + ". Cannot apply property "
                    + propertyName + "=(" + value.getClass() + ") " + value + " to the field (" + type + ") "
                    + field.getName(), e);
        } catch (final IllegalAccessException e) {
            throw new RuntimeException("Wrong policy. Cannot access fields");
        }
    }
}
