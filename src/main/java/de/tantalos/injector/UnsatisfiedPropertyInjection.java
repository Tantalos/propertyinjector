package de.tantalos.injector;

public class UnsatisfiedPropertyInjection extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 4680653629098764751L;

    public UnsatisfiedPropertyInjection() {
        super();
    }

    public UnsatisfiedPropertyInjection(Exception e) {
        super(e);
    }

    public UnsatisfiedPropertyInjection(String message, Exception e) {
        super(message, e);
    }

    public UnsatisfiedPropertyInjection(String message) {
        super(message);
    }
}
