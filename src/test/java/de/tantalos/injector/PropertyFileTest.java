package de.tantalos.injector;

import java.net.URL;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.base.Preconditions;

public class PropertyFileTest {

    @Test
    public void testPropertyFileInjector() {
        @PropertyFile(name = "test_config.yml")
        class AnyConfig {
            @Property(name = "stringlist")
            public List<String> stringList;
            @Property(name = "boolean")
            public Boolean booleanVal;
            @Property(name = "integer")
            public Integer integer;
        }

        AnyConfig anyConfig = new AnyConfig();

        new PropertyInjector().inject(anyConfig);

        Assert.assertNotNull(anyConfig.stringList);
        Assert.assertNotNull(anyConfig.booleanVal);
        Assert.assertNotNull(anyConfig.integer);
    }

    @Test(expected = UnsatisfiedPropertyInjection.class)
    public void testUnknownFile() {
        @PropertyFile(name = "unknown_config.yml")
        class AnyConfig {
        }

        URL file = getClass().getResource("/unknown_config.yml");
        Preconditions.checkArgument(file == null);

        AnyConfig anyConfig = new AnyConfig();
        new PropertyInjector().inject(anyConfig);
    }
}
