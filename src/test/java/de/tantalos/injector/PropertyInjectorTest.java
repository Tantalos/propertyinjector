package de.tantalos.injector;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Preconditions;

public class PropertyInjectorTest {
    private YamlConfiguration config;

    @Before
    public void setup() throws InvalidConfigurationException, FileNotFoundException, IOException {
        config = new YamlConfiguration();
        final File configFile = new File("./src/test/resources/test_config.yml");
        config.load(configFile);

    }

    @Test
    public void testIntegerInjection() {
        class IntContainer {
            @Property(name = "integer")
            public Integer integer;
            @Property(name = "integer")
            public int primitiveInteger;
        }

        Preconditions.checkNotNull(config);

        final IntContainer intContainer = new IntContainer();
        new PropertyInjector().inject(intContainer, config);

        final Integer readInt = config.getInt("integer");

        assertEquals(readInt, intContainer.integer);
        assertEquals((int) readInt, intContainer.primitiveInteger);

        // because default value is 0 and must differ
        Preconditions.checkArgument(not(equalTo(0)).matches(intContainer.primitiveInteger));
    }

    @Test
    public void testLongInjection() {
        class LongContainer {
            @Property(name = "long")
            public Long longVal;
            @Property(name = "long")
            public long primitiveLong;
        }

        Preconditions.checkNotNull(config);

        final LongContainer longContainer = new LongContainer();
        new PropertyInjector().inject(longContainer, config);

        final Long readLong = config.getLong("long");

        assertEquals(readLong, longContainer.longVal);
        assertEquals((long) readLong, longContainer.primitiveLong);

        // because default value is 0 and must differ
        Preconditions.checkArgument(not(equalTo(0l)).matches(longContainer.primitiveLong));
    }

    @Test
    public void testShortInjection() {
        class ShortContainer {
            @Property(name = "short")
            public short primitiveShort;
        }

        Preconditions.checkNotNull(config);

        final ShortContainer shortContainer = new ShortContainer();
        new PropertyInjector().inject(shortContainer, config);

        assertThat(shortContainer.primitiveShort, equalTo((short) 017));

        // because default value is 0 and must differ
        Preconditions.checkArgument(not(equalTo((short) 00)).matches(shortContainer.primitiveShort));
    }

    @Test
    public void testBooleanInjection() {
        class BooleanContainer {
            @Property(name = "boolean")
            public Boolean booleanVal;
            @Property(name = "boolean")
            public boolean primitiveBoolean;
        }

        final BooleanContainer booleanContainer = new BooleanContainer();

        Preconditions.checkNotNull(config);

        new PropertyInjector().inject(booleanContainer, config);

        final Boolean readBoolean = config.getBoolean("boolean");

        assertEquals(booleanContainer.primitiveBoolean, booleanContainer.booleanVal);
        assertEquals(readBoolean, booleanContainer.booleanVal);

        // because default value is 0 and must differ
        Preconditions.checkArgument(not(equalTo(false)).matches(booleanContainer.primitiveBoolean));

    }

    @Test
    public void testList() {
        class ListContainer {
            @Property(name = "stringlist")
            public List<String> stringList;
            @Property(name = "intlist")
            public List<String> integerList;

        }

        Preconditions.checkNotNull(config);
        List<String> stringList = config.getStringList("stringlist");
        List<Integer> intList = config.getIntegerList("intlist");
        Preconditions.checkNotNull(stringList);
        Preconditions.checkNotNull(intList);

        ListContainer listContainer = new ListContainer();
        new PropertyInjector().inject(listContainer, config);

        assertEquals(listContainer.stringList, stringList);
        assertEquals(listContainer.integerList, intList);

    }

}
