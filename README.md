#Property Injector

Property injector is a utility to easily load your configuration items to your code. The properies are directly binded via Annotation to the class attributes.

#Example


Java class with properties you want to inject from configuration

```
#!Java

@PropertyFile(name="config.yml")
class ConfigValues {
    @Property(name = "varname")
    public Boolean booleanVal;
    @Property(name = "intvariable")
    public Integer othervarname;
    @Property(name = "list")
    public List<String> alsoLists;
    @Property(name = "intlist")
    public List<String> intLists;
}

```

Yaml configuration file

```
#!YML

varname: true
intvariable: 20
list:
    - first
    - second
    - third
intlist:
    - 4
    - 6
    - 100
```

To load this configuration you need to create a PropertyInjector and call the inject method on the data object

```
#!Java

@Override
public void onEnable() {
    ConfigValues configValues = new ConfigValues();
    new PropertyInjector().inject(configValues);

    configValues.othervarname;  // 20
    configValues.intlist        // [4, 6, 100]
    ...
}

```